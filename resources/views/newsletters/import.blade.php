@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Importar</div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-warning" role="alert">{{ $error }}</div>
                                        @endforeach
                                @endif

    						{!! Form::open(['url' => 'newsletters','method'=>'post','files' => true]) !!}
                                    <div class="form-group">
                                        {!! Form::label('arquivo','Arquivo CSV') !!}
                                        {!! Form::file('arquivo','',['class' => 'form-control','placeholder' => 'Arquivo']) !!}

                                        @if ($errors->has('arquivo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('arquivo') }}</strong>
                                            </span>
                                        @endif								
                                        <p class="help-block">Anexe o arquivo para importação</p>
                                    </div>		
                                    <div class="form-group">
                                        {!! Form::submit('Cadastrar',['class' => 'btn btn-default']) !!}
                                    </div>
                                {!! Form::close() !!}		
                            </div>
                    </div>
                </div>		
            </div>
            
        </div>
@endsection