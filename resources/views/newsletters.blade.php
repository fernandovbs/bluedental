@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Newsletter</h2>
                    <a href="{{ url('/newsletters/create') }}">
                        <button class="btn btn-primary">Importar Registros</button>
                    </a>
              </div>
            <div>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if(count($newsletterClients) > 0)
                        <table class="table">
                            <tread>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                </tr>
                            </tread>
                        @foreach ($newsletterClients as $newsletterClient)
                            <tr>
                                <td>{{$newsletterClient->nome}}</td>
                                <td>{{$newsletterClient->email}}</td>
                            </tr>
                        @endforeach
                        </table>
                    @else
                        <h3>Não existem registros importados</h3>
                    @endif
                </div>
            </div>
        </div>
@endsection