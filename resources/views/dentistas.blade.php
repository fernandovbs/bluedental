@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Dentistas</h2>
                    <a href="{{ url('/dentistas/create') }}">
                        <button class="btn btn-primary">Cadastrar Dentista</button>
                    </a>
              </div>
            <div>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if(count($dentistas) > 0)
                        <table class="table">
                            <tread>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Visualizar Horários</th>
                                    <th>Excluir</th>
                                </tr>
                            </tread>
                        @foreach ($dentistas as $dentista)
                            <tr>
                                <td>{{$dentista->nome}}</td>
                                <td>{{$dentista->email}}</td>
                                <td><a href="{{ route('dentistas.show', $dentista->id) }}">Visualizar</a></td>
                                <td>
                                    <form action="{{ route('dentistas.destroy', $dentista->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button>Apagar</button>
                                    </form>                                
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    @else
                        <h3>Não existem dentistas cadastrados</h3>
                    @endif
                </div>
            </div>
        </div>
@endsection