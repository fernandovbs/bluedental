@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Novo Dentista</div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-warning" role="alert">{{ $error }}</div>
                                        @endforeach
                                @endif

                                {!! Form::open(['url' => 'dentistas','method'=>'post']) !!}
                                    <div class="form-group">
                                        {!! Form::label('nome','Nome') !!}
                                        {!! Form::text('nome','',['class' => 'form-control','placeholder' => 'nome']) !!}

                                        @if ($errors->has('nome'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nome') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email','Email') !!}
                                        {!! Form::text('email','',['class' => 'form-control','placeholder' => 'email']) !!}

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        {!! Form::submit('Cadastrar',['class' => 'btn btn-default']) !!}
                                    </div>
                                {!! Form::close() !!}		
                            </div>
                    </div>
                </div>		
            </div>
            
        </div>
@endsection