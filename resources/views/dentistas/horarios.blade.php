@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Horários</h2>
                    <a href="{{ url('/horarios/create') }}">
                        <button class="btn btn-primary">Cadastrar horário</button>
                    </a>
              </div>
            <div>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if(count($horarios) > 0)
                        <table class="table">
                            <tread>
                                <tr>
                                    <th>Unidade</th>
                                    <th>Dia</th>
                                    <th>Início</th>
                                    <th>Término</th>
                                    <th>Excluir Horário</th>
                                </tr>
                            </tread>
                        @foreach ($horarios as $horario)
                            <tr>
                                <td>{{$horario->descricao}}</td>
                                <td>{{$horario->dia}}</td>
                                <td>{{$horario->horario_inicio}}</td>
                                <td>{{$horario->horario_termino}}</td>
                                <td>
                                    <form action="{{ route('horarios.destroy', $horario->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button>Apagar</button>
                                    </form>                                
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    @else
                        <h3>Não existem horários cadastrados para este dentista</h3>
                    @endif
                </div>
            </div>
        </div>
@endsection