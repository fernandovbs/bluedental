@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Unidades</h2>
                    <a href="{{ url('/unidades/create') }}">
                        <button class="btn btn-primary">Cadastrar Unidade</button>
                    </a>
              </div>
            <div>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if(count($unidades) > 0)
                        <table class="table">
                            <tread>
                                <tr>
                                    <th>Descrição</th>
                                    <th>Excluir</th>
                                </tr>
                            </tread>
                        @foreach ($unidades as $unidade)
                            <tr>
                                <td>{{$unidade->descricao}}</td>
                                <td>
                                    <form action="{{ route('unidades.destroy', $unidade->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button>Apagar</button>
                                    </form>                                
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    @else
                        <h3>Não existem unidades cadastradas</h3>
                    @endif
                </div>
            </div>
        </div>
@endsection