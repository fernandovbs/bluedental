@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Novo Horário</div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-warning" role="alert">{{ $error }}</div>
                                        @endforeach
                                @endif
                                @php //dd($unidades);@endphp
                                {!! Form::open(['url' => 'horarios','method'=>'post']) !!}
                                    <div class="form-group">
                                        {!! Form::label('unidade_id','Unidade') !!}
                                        {!! Form::select('unidade_id',$unidades,['class' => 'form-control']) !!}

                                        @if ($errors->has('unidade_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('unidade_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('dentista_id','Dentista') !!}
                                        {!! Form::select('dentista_id',$dentistas,['class' => 'form-control']) !!}

                                        @if ($errors->has('dentista_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dentista_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>                  
                                    <div class="form-group">
                                        {!! Form::label('dia','Dia da Semana') !!}
                                        {!! Form::select('dia',['Segunda-feira' => 'Segunda-feira','Terça-feira' => 'Terça-feira','Quarta-feira' => 'Quarta-feira','Quinta-feira' => 'Quinta-feira','Sexta-feira' => 'Sexta-feira','Sábado' => 'Sábado','Domingo' => 'Domingo'],['class' => 'form-control']) !!}

                                        @if ($errors->has('dentista'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dentista') }}</strong>
                                            </span>
                                        @endif
                                    </div>                                                             
                                    <div class="form-group">
                                        {!! Form::label('horario_inicio','Início') !!}
                                        {!! Form::text('horario_inicio','',['class' => 'form-control','placeholder' => 'HH:MM']) !!}

                                        @if ($errors->has('horario_inicio'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('horario_inicio') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('horario_termino','Término') !!}
                                        {!! Form::text('horario_termino','',['class' => 'form-control','placeholder' => 'HH:MM']) !!}

                                        @if ($errors->has('horario_termino'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('horario_termino') }}</strong>
                                            </span>
                                        @endif
                                    </div>                                                                                   
                                    <div class="form-group">
                                        {!! Form::submit('Cadastrar',['class' => 'btn btn-default']) !!}
                                    </div>
                                {!! Form::close() !!}		
                            </div>
                    </div>
                </div>		
            </div>
            
        </div>
@endsection