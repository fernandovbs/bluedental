@extends('app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Nova Unidade</div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-warning" role="alert">{{ $error }}</div>
                                        @endforeach
                                @endif

                                {!! Form::open(['url' => 'unidades','method'=>'post']) !!}
                                    <div class="form-group">
                                        {!! Form::label('descricao','Descrição') !!}
                                        {!! Form::text('descricao','',['class' => 'form-control','placeholder' => 'Descrição']) !!}

                                        @if ($errors->has('descricao'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('descricao') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Cadastrar',['class' => 'btn btn-default']) !!}
                                    </div>
                                {!! Form::close() !!}		
                            </div>
                    </div>
                </div>		
            </div>
            
        </div>
@endsection