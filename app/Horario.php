<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    //
    protected $fillable = ['unidade_id','dentista_id','horario_inicio','horario_termino','dia'];
}
