<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Dentista;
use App\Horarios;

class DentistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dentistas = Dentista::all();
    
        return view('dentistas',['dentistas' => $dentistas ]);        	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dentistas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate( $request,[   
                'nome' => 'required|max:100',
                'email' => 'required|email|max:255|unique:dentistas',
            ]);
        
        $dados = $request->all();
        
        if(Dentista::create($dados))
        {
            return redirect('dentistas');
        }else
        {
            return view('dentistas.create',['errors' => array('Não foi possível cadastrar o novo dentista')]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $horarios = DB::table('dentistas')
        ->join('horarios','dentistas.id','=','horarios.dentista_id')
        ->join('unidades','horarios.unidade_id','=','unidades.id')
        ->where('horarios.dentista_id',$id)
        ->select('unidades.descricao','horarios.dia','horarios.horario_inicio','horarios.horario_termino','horarios.id','dentistas.nome')
        ->get();

        return view('dentistas.horarios',['horarios' => $horarios]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Dentista::destroy($id);
        return redirect('dentistas');
    }
}
