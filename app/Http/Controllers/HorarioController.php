<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horario;
use App\Unidade;
use App\Dentista;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return redirect('horarios/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $unidades = Unidade::select('id','descricao')->get();

        $dentistas = Dentista::select('id','nome')->get();
        
        $unidades = $unidades->mapWithKeys(function($item){

            return [$item->descricao => $item->id];
        })->flip();

        $dentistas = $dentistas->mapWithKeys(function($item){

            return [$item['nome'] => $item['id']];
        })->flip();

        return view('horarios.create',['unidades' => $unidades->all(),'dentistas' => $dentistas->all() ]);      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $this->validate( $request,[   
                'unidade_id' => 'required|exists:unidades,id',
                'dentista_id' => 'required|exists:dentistas,id',
                'horario_inicio' => 'required|date_format:H:i',
                'horario_termino' => 'required|date_format:H:i',
                'dia' => 'required|max:15',
            ]);
        
        $dados = $request->all();
        
        if(Horario::create($dados))
        {
            return redirect('horarios/create');
        }else
        {
            return redirect('horarios/create');
        }
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Horario::destroy($id);
        return redirect('dentistas');       
    }
}
