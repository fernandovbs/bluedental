<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newsletter;
class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $newsletterClients = Newsletter::all();
    
        return view('newsletters',['newsletterClients' => $newsletterClients ]);                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('newsletters.import'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $arquivo = $request->file('arquivo');

        if($arquivo)
        {
            $content = file($arquivo->path());

            foreach($content as $linha){
                $linha = explode(',',trim(preg_replace('/\s\s+/', ' ', $linha)));

                if (filter_var($linha[1], FILTER_VALIDATE_EMAIL))
                {

                    $dados['nome'] = $linha[0];
                    $dados['email'] = $linha[1];
                
                    Newsletter::create($dados);
                    unset($dados);
                }
            }
        }
        return redirect('newsletters');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
